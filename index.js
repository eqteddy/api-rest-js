const express = require("express");
const { Pool } = require("pg");

const app = express();
const port = 3000;

const pool = new Pool({
    /*
    user: "postgres",
    password: "postgres",
    host: "localhost",
    database: "userdb",
    */
    port:"5432",
    user: "userpg",
    password: "ZzXWja2OQviPkU9dCyZoeA7G9jw8MrHt",
    host: "dpg-cjqda3ojbais73a6ctc0-a",
    database: "userdb_b3qu",
});

//// MODELO
class Model {
    async getUsuarios() {
        const { rows } = await pool.query("select * from usuarios");
        return rows;
    }
    async getUsuario(id) {
        const { rows } = await pool.query("select * from usuarios where id=$1",[id]);
        return rows[0];
    }
    async addUsuario(ci,nombre,primer_apellido,segundo_apellido,fecha_nac){
        await pool.query("insert into usuarios (ci,nombre,primer_apellido,segundo_apellido,fecha_nac) values ($1,$2,$3,$4,$5)",[ci,nombre,primer_apellido,segundo_apellido,fecha_nac]);
    }
    async updateUsuario(id,ci,nombre,primer_apellido,segundo_apellido,fecha_nac){
        await pool.query("update usuarios set ci = $1,nombre =$2,primer_apellido = $3,segundo_apellido =$4,fecha_nac = $5 where id=$6",[ci,nombre,primer_apellido,segundo_apellido,fecha_nac,id]);
    }
    async deleteUsuario(id) {
        await pool.query("delete from usuarios where id = $1",[id])
    }
    async avgEdad() {
        const { rows } = await pool.query("SELECT AVG(EXTRACT(YEAR FROM AGE(NOW(),fecha_nac))) as promedioEdad FROM usuarios");
        //console.log(rows);
        return rows[0];
    }
}

//// CONTROLADOR

class Controller {
    constructor(model) {
        this.model = model;
    }
    async getUsuarios(req,res) {
        const data = await this.model.getUsuarios();
        res.send(data);
    }
    async getUsuario(req,res) {
        const id = req.params.id;
        const data = await this.model.getUsuario(id);
        res.send(data);
    }
    async addUsuario(req,res) {
        const nombre = req.body.nombre;
        const primer_apellido = req.body.primer_apellido;
        const segundo_apellido = req.body.segundo_apellido;
        const ci = req.body.ci;
        const fecha_nac = req.body.fecha_nac;

        await this.model.addUsuario(ci,nombre,primer_apellido,segundo_apellido,fecha_nac);
        res.sendStatus(201);
    }
    async updateUsuario(req,res) {
        const id = req.params.id;
        const nombre = req.body.nombre;
        const primer_apellido = req.body.primer_apellido;
        const segundo_apellido = req.body.segundo_apellido;
        const ci = req.body.ci;
        const fecha_nac = req.body.fecha_nac;

        await this.model.updateUsuario(id,ci,nombre,primer_apellido,segundo_apellido,fecha_nac);
        res.sendStatus(200);       
    }
    async deleteUsuario(req,res) {
        const id = req.params.id;
        await this.model.deleteUsuario(id);
        res.sendStatus(200);
    }
    async avgEdad(req,res) {
        const data = await this.model.avgEdad();
        res.send(data);
    }
    async version(req,res) {
        const packageJSON = require('./package.json');
        const data = {
            nameSystem: packageJSON.name, 
            version: packageJSON.version,
            developer: packageJSON.author,
            email: packageJSON.email,
        }
        res.send(data);
    }
}

//Instanciacio
const model = new Model();
const controller = new Controller(model);

app.use(express.json());

app.get("/usuarios/promedio-edad", controller.avgEdad.bind(controller));
app.get("/usuarios", controller.getUsuarios.bind(controller));
app.get("/usuarios/:id", controller.getUsuario.bind(controller));
app.post("/usuarios", controller.addUsuario.bind(controller));
app.put("/usuarios/:id", controller.updateUsuario.bind(controller));
app.delete("/usuarios/:id", controller.deleteUsuario.bind(controller));

app.get("/estado", controller.version.bind(controller));


app.listen(port, () => {
  console.log(`Este servidor se ejecuta en http://localhost:${port}`);
});
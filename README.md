# DIPLOMADO FULLSTACK DEVELOPER - MÓDULO III

## API REST CON JS

## Instalación
### Base de datos

1. Se debe crear la Base de Datos userdb
2. Se debe ejecutar la creacion de la tabla y datos segun el siguiente script:

```sql

create table usuarios (
	id serial primary key,
	ci varchar(12) not null,
	nombre varchar(50) not null,
	primer_apellido varchar(50) null,
	segundo_apellido varchar (50) null,
	fecha_nac date not null
)

insert into usuarios (ci,nombre,primer_apellido,segundo_apellido,fecha_nac)
values
 ('111111','Pablo','Ramos','Choque','1990-01-30'),
 ('222222','Juan','Perez','','2000-11-13'),
 ('333333','Pedro','Machicado','Gomez','1988-03-25'),
 ('444444','Eddy','Quelca','Tancara','1980-05-15')
```
# Aplicación

1. se debe clonar la aplicación
```cli
git clone https://gitlab.com/eqteddy/api-rest-js.git
```
2. Ejecutar la aplicación
```ps
node index.js
```

# Publicado
Se publico en https://api-rest-js.onrender.com/

1. GET https://api-rest-js.onrender.com/usuarios
2. GET https://api-rest-js.onrender.com/usuarios/:id
3. POST https://api-rest-js.onrender.com/usuarios
4. PUT https://api-rest-js.onrender.com/usuarios/:id
5. DELETE https://api-rest-js.onrender.com/usuarios/:id
6. GET https://api-rest-js.onrender.com/usuarios/promedio-edad
7. GET https://api-rest-js.onrender.com/estado
```cli
{"nameSystem":"api-rest-js","version":"1.0.0","developer":"Eddy Eufracio Quelca Tancara","email":"eqteddy@gmail.com"}
```
